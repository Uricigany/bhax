#include <stdio.h>
#include <string.h>

int
main (int argc, char **argv)
{
  int readByte;  
    
  int keyIndex = 0;

  int keyLength = strlen (argv[1]);

  
  while((readByte = getchar()) != EOF)
  {
      readByte = readByte ^ argv[1][keyIndex];
      keyIndex = (keyIndex + 1) % keyLength;
      
      putchar(readByte);
  }
  
  return 0;
}
